package captcha

import "time"

type Verify struct {
	Token  string `json:"token"`
	Digits string `json:"digits"`
}

type TokenImagesCodeVerify struct {
	Time   time.Time
	Code   string
	Answer string
}

type Lang string

const (
	LangEN Lang = "en"
	LangJA Lang = "ja"
	LangRU Lang = "ru"
	LangZH Lang = "zh"
)
