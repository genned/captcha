package captcha

import (
	"bytes"
	"errors"

	"github.com/wzshiming/captcha"
)

// CaptchaService 图形和语音验证码 #path:"/captcha"#
// 1. POST /captcha/token 获取 Token
// 2. GET /capture/{token}.png 获取图形验证码 or GET /capture/{token}.wav 获取语音证码
// 3. POST /captcha 验证 注意 这个接口仅用于测试
// 这些参数也会存在需要图形验证的接口上
// 所有 token 验证过一次就失效
// 失效后 从1 重新开始
type CaptchaService struct {
	length        int
	width, height int
}

func NewCaptchaService(store captcha.Store, length int, width int, height int) *CaptchaService {
	if store != nil {
		captcha.SetCustomStore(store)
	}
	return &CaptchaService{
		length: length,
		width:  width,
		height: height,
	}
}

// Token #route:"POST /token"#
func (c *CaptchaService) Token() (token string, err error) {
	token = captcha.NewLen(c.length)
	return token, nil
}

// TokenPNG #route:"GET /{token}.png"#
func (c *CaptchaService) TokenPNG(token string) (png []byte /* #content:"image/png"# */, err error) {
	buf := bytes.NewBuffer(nil)
	err = captcha.WriteImage(buf, token, c.width, c.height)
	if err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}

// TokenWAV #route:"GET /{token}.wav"#
func (c *CaptchaService) TokenWAV(token string, lang Lang) (wav []byte /* #content:"audio/x-wav"# */, err error) {
	buf := bytes.NewBuffer(nil)
	err = captcha.WriteAudio(buf, token, string(lang))
	if err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}

// Verify 验证验证码 #route:"POST /"#
func (c *CaptchaService) Verify(verify *Verify, clear int) (err error) {
	ok := captcha.VerifyString(verify.Token, verify.Digits, clear != 0)
	if !ok {
		return errors.New("验证码验证失败")
	}
	return nil
}
