package mgostore

import (
	"encoding/gob"
	"time"

	"github.com/globalsign/mgo"
)

type MgoStore struct {
	db         *mgo.Collection
	expiration time.Duration
}

func init() {
	gob.Register(&pair{})
}

type pair struct {
	ID     string    `bson:"_id"`
	Time   time.Time `bson:"time"`
	Digits []byte    `bson:"digits"`
}

func NewMgoStore(db *mgo.Collection, expiration time.Duration) *MgoStore {
	return &MgoStore{
		db:         db,
		expiration: expiration,
	}

}

func (s *MgoStore) Set(id string, digits []byte) {
	s.db.Insert(&pair{
		ID:     id,
		Time:   time.Now(),
		Digits: digits,
	})
}

func (s *MgoStore) Get(id string, clear bool) (digits []byte) {
	p := &pair{}
	err := s.db.FindId(id).One(&p)
	if err != nil {
		return
	}

	if p.Time.Add(s.expiration).Before(time.Now()) {
		s.db.RemoveId(id)
		return
	}

	digits = p.Digits

	if clear {
		s.db.RemoveId(id)
	}

	return

}
