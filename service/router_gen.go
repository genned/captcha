// Code generated; DO NOT EDIT.
// file service/router_gen.go

package service

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"github.com/wzshiming/gen/ui/swaggerui"
	gitlabComGennedCaptchaServiceCaptcha "gitlab.com/genned/captcha/service/captcha"
	"io/ioutil"
	"net/http"
	"os"
	"unsafe"
)

// Router is all routing for package
// generated do not edit.
func Router() http.Handler {
	router := mux.NewRouter()

	// CaptchaService Define the method scope
	var _varCaptchaService gitlabComGennedCaptchaServiceCaptcha.CaptchaService
	RouteCaptchaService(router, &_varCaptchaService)

	return router
}

// RouteCaptchaService is routing for CaptchaService
func RouteCaptchaService(router *mux.Router, _varCaptchaService *gitlabComGennedCaptchaServiceCaptcha.CaptchaService) *mux.Router {
	if router == nil {
		router = mux.NewRouter()
	}

	// Registered routing POST /captcha
	router.Methods("POST").Path("/captcha").
		HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			_operationPostCaptcha(_varCaptchaService, w, r)
		})

	// Registered routing GET /captcha/{token}.wav
	router.Methods("GET").Path("/captcha/{token}.wav").
		HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			_operationGetCaptchaTokenWav(_varCaptchaService, w, r)
		})

	// Registered routing GET /captcha/{token}.png
	router.Methods("GET").Path("/captcha/{token}.png").
		HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			_operationGetCaptchaTokenPng(_varCaptchaService, w, r)
		})

	// Registered routing GET /captcha
	router.Methods("GET").Path("/captcha").
		HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			_operationGetCaptcha(_varCaptchaService, w, r)
		})

	return router
}

// _requestQueryLang Parsing the query for of lang
func _requestQueryLang(w http.ResponseWriter, r *http.Request) (lang gitlabComGennedCaptchaServiceCaptcha.Lang, err error) {

	var _lang = r.URL.Query().Get("lang")
	lang = gitlabComGennedCaptchaServiceCaptcha.Lang(_lang)
	return
}

// _requestPathToken Parsing the path for of token
func _requestPathToken(w http.ResponseWriter, r *http.Request) (token string, err error) {

	var _token = mux.Vars(r)["token"]
	token = string(_token)
	return
}

// _requestPathToken_1 Parsing the path for of token
func _requestPathToken_1(w http.ResponseWriter, r *http.Request) (token string, err error) {

	var _token = mux.Vars(r)["token"]
	token = string(_token)
	return
}

// _requestBodyVerify Parsing the body for of verify
func _requestBodyVerify(w http.ResponseWriter, r *http.Request) (verify *gitlabComGennedCaptchaServiceCaptcha.Verify, err error) {

	var _body []byte
	_body, err = ioutil.ReadAll(r.Body)
	if err == nil {
		r.Body.Close()
		err = json.Unmarshal(_body, &verify)
	}
	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}

	return
}

// _operationPostCaptcha Is the route of Verify
func _operationPostCaptcha(s *gitlabComGennedCaptchaServiceCaptcha.CaptchaService, w http.ResponseWriter, r *http.Request) {

	// Parsing verify.
	_verify, err := _requestBodyVerify(w, r)
	if err != nil {
		return
	}

	// Call Verify.
	_err := s.Verify(_verify)

	// Response code 400 Bad Request for err.
	if _err != nil {
		http.Error(w, _err.Error(), 400)

		return
	}

	w.WriteHeader(204)
	w.Write(nil)

	return
}

// _operationGetCaptchaTokenWav Is the route of TokenWAV
func _operationGetCaptchaTokenWav(s *gitlabComGennedCaptchaServiceCaptcha.CaptchaService, w http.ResponseWriter, r *http.Request) {

	// Parsing token.
	_token, err := _requestPathToken(w, r)
	if err != nil {
		return
	}

	// Parsing lang.
	_lang, err := _requestQueryLang(w, r)
	if err != nil {
		return
	}

	// Call TokenWAV.
	_wav, _err := s.TokenWAV(_token, _lang)

	// Response code 200 OK for wav.
	if _wav != nil {
		data := _wav

		w.Header().Set("Content-Type", "audio/x-wav")
		w.WriteHeader(200)
		w.Write(data)

		return
	}

	// Response code 400 Bad Request for err.
	if _err != nil {
		http.Error(w, _err.Error(), 400)

		return
	}

	data := _wav

	w.Header().Set("Content-Type", "audio/x-wav")
	w.WriteHeader(200)
	w.Write(data)

	return
}

// _operationGetCaptchaTokenPng Is the route of TokenPNG
func _operationGetCaptchaTokenPng(s *gitlabComGennedCaptchaServiceCaptcha.CaptchaService, w http.ResponseWriter, r *http.Request) {

	// Parsing token.
	_token, err := _requestPathToken_1(w, r)
	if err != nil {
		return
	}

	// Call TokenPNG.
	_png, _err := s.TokenPNG(_token)

	// Response code 200 OK for png.
	if _png != nil {
		data := _png

		w.Header().Set("Content-Type", "image/png")
		w.WriteHeader(200)
		w.Write(data)

		return
	}

	// Response code 400 Bad Request for err.
	if _err != nil {
		http.Error(w, _err.Error(), 400)

		return
	}

	data := _png

	w.Header().Set("Content-Type", "image/png")
	w.WriteHeader(200)
	w.Write(data)

	return
}

// _operationGetCaptcha Is the route of Token
func _operationGetCaptcha(s *gitlabComGennedCaptchaServiceCaptcha.CaptchaService, w http.ResponseWriter, r *http.Request) {

	// Call Token.
	_token, _err := s.Token()

	// Response code 200 OK for token.
	if _token != "" {
		data, err := json.Marshal(_token)
		if err != nil {
			http.Error(w, err.Error(), 500)
			return
		}

		w.Header().Set("Content-Type", "application/json; charset=utf-8")
		w.WriteHeader(200)
		w.Write(data)

		return
	}

	// Response code 400 Bad Request for err.
	if _err != nil {
		http.Error(w, _err.Error(), 400)

		return
	}

	data, err := json.Marshal(_token)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(200)
	w.Write(data)

	return
}

// {
//  "openapi": "3.0.1",
//  "info": {
//   "title": "OpenAPI Demo",
//   "description": "Automatically generated",
//   "contact": {
//    "name": "wzshiming",
//    "url": "https://github.com/wzshiming/gen"
//   },
//   "version": "0.0.1"
//  },
//  "servers": [
//   {
//    "url": "/"
//   },
//   {
//    "url": "{scheme}{host}{port}{path}",
//    "variables": {
//     "host": {
//      "enum": [
//       "localhost"
//      ],
//      "default": "localhost"
//     },
//     "path": {
//      "enum": [
//       "/"
//      ],
//      "default": "/"
//     },
//     "port": {
//      "enum": [
//       ""
//      ],
//      "default": ""
//     },
//     "scheme": {
//      "enum": [
//       "http://",
//       "https://"
//      ],
//      "default": "http://"
//     }
//    }
//   }
//  ],
//  "paths": {
//   "/captcha": {
//    "get": {
//     "tags": [
//      "captcha"
//     ],
//     "description": "Token #route:\"GET /\"#",
//     "responses": {
//      "200": {
//       "content": {
//        "application/json": {
//         "schema": {
//          "type": "string"
//         }
//        }
//       }
//      },
//      "400": {
//       "content": {
//        "text/plain": {
//         "schema": {
//          "type": "string",
//          "format": "error"
//         }
//        }
//       }
//      }
//     }
//    },
//    "post": {
//     "tags": [
//      "captcha"
//     ],
//     "description": "Verify 该接口仅用于测试 所有 token 验证过一次就失效 #route:\"POST /\"#",
//     "requestBody": {
//      "$ref": "#/components/requestBodies/verify.458b"
//     },
//     "responses": {
//      "400": {
//       "content": {
//        "text/plain": {
//         "schema": {
//          "type": "string",
//          "format": "error"
//         }
//        }
//       }
//      }
//     }
//    }
//   },
//   "/captcha/{token}.png": {
//    "get": {
//     "tags": [
//      "captcha"
//     ],
//     "description": "TokenPNG #route:\"GET /{token}.png\"#",
//     "parameters": [
//      {
//       "$ref": "#/components/parameters/token.bd2d"
//      }
//     ],
//     "responses": {
//      "200": {
//       "description": "#content:\"image/png\"#",
//       "content": {
//        "image/png": {}
//       }
//      },
//      "400": {
//       "content": {
//        "text/plain": {
//         "schema": {
//          "type": "string",
//          "format": "error"
//         }
//        }
//       }
//      }
//     }
//    }
//   },
//   "/captcha/{token}.wav": {
//    "get": {
//     "tags": [
//      "captcha"
//     ],
//     "description": "TokenWAV #route:\"GET /{token}.wav\"#",
//     "parameters": [
//      {
//       "$ref": "#/components/parameters/token.511c"
//      },
//      {
//       "$ref": "#/components/parameters/lang.c10c"
//      }
//     ],
//     "responses": {
//      "200": {
//       "description": "#content:\"audio/x-wav\"#",
//       "content": {
//        "audio/x-wav": {}
//       }
//      },
//      "400": {
//       "content": {
//        "text/plain": {
//         "schema": {
//          "type": "string",
//          "format": "error"
//         }
//        }
//       }
//      }
//     }
//    }
//   }
//  },
//  "components": {
//   "schemas": {
//    "CaptchaService.64f0": {
//     "type": "object",
//     "description": "CaptchaService 图形和语音验证码 #path:\"/captcha\"#\n1. GET /captcha 获取 Token\n2. GET /capture/{token}.png 获取图形验证码 or GET /capture/{token}.wav 获取语音证码\n3. POST /captcha 验证 注意 这个接口仅用于测试\n这些参数也会存在需要图形验证的接口上\n所有 token 验证过一次就失效\n失效后 从1 重新开始"
//    },
//    "Lang.5402": {
//     "enum": [
//      "zh",
//      "ru",
//      "ja",
//      "en"
//     ],
//     "type": "string"
//    },
//    "Verify.e78c": {
//     "type": "object",
//     "properties": {
//      "digits": {
//       "type": "string"
//      },
//      "token": {
//       "type": "string"
//      }
//     }
//    }
//   },
//   "responses": {
//    "err.68d3": {
//     "content": {
//      "text/plain": {
//       "schema": {
//        "type": "string",
//        "format": "error"
//       }
//      }
//     }
//    },
//    "png.b978": {
//     "description": "#content:\"image/png\"#",
//     "content": {
//      "image/png": {}
//     }
//    },
//    "token.530b": {
//     "content": {
//      "application/json": {
//       "schema": {
//        "type": "string"
//       }
//      }
//     }
//    },
//    "wav.4e5f": {
//     "description": "#content:\"audio/x-wav\"#",
//     "content": {
//      "audio/x-wav": {}
//     }
//    }
//   },
//   "parameters": {
//    "lang.c10c": {
//     "name": "lang",
//     "in": "query",
//     "schema": {
//      "$ref": "#/components/schemas/Lang.5402"
//     }
//    },
//    "token.511c": {
//     "name": "token",
//     "in": "path",
//     "required": true,
//     "schema": {
//      "type": "string"
//     }
//    },
//    "token.bd2d": {
//     "name": "token",
//     "in": "path",
//     "required": true,
//     "schema": {
//      "type": "string"
//     }
//    }
//   },
//   "requestBodies": {
//    "verify.458b": {
//     "content": {
//      "application/json": {
//       "schema": {
//        "$ref": "#/components/schemas/Verify.e78c"
//       }
//      }
//     }
//    }
//   }
//  },
//  "tags": [
//   {
//    "name": "captcha",
//    "description": "CaptchaService 图形和语音验证码 #path:\"/captcha\"#\n1. GET /captcha 获取 Token\n2. GET /capture/{token}.png 获取图形验证码 or GET /capture/{token}.wav 获取语音证码\n3. POST /captcha 验证 注意 这个接口仅用于测试\n这些参数也会存在需要图形验证的接口上\n所有 token 验证过一次就失效\n失效后 从1 重新开始"
//   }
//  ]
// }
var OpenAPI = `{"openapi":"3.0.1","info":{"title":"OpenAPI Demo","description":"Automatically generated","contact":{"name":"wzshiming","url":"https://github.com/wzshiming/gen"},"version":"0.0.1"},"servers":[{"url":"/"},{"url":"{scheme}{host}{port}{path}","variables":{"host":{"enum":["localhost"],"default":"localhost"},"path":{"enum":["/"],"default":"/"},"port":{"enum":[""],"default":""},"scheme":{"enum":["http://","https://"],"default":"http://"}}}],"paths":{"/captcha":{"get":{"tags":["captcha"],"description":"Token #route:\"GET /\"#","responses":{"200":{"content":{"application/json":{"schema":{"type":"string"}}}},"400":{"content":{"text/plain":{"schema":{"type":"string","format":"error"}}}}}},"post":{"tags":["captcha"],"description":"Verify 该接口仅用于测试 所有 token 验证过一次就失效 #route:\"POST /\"#","requestBody":{"$ref":"#/components/requestBodies/verify.458b"},"responses":{"400":{"content":{"text/plain":{"schema":{"type":"string","format":"error"}}}}}}},"/captcha/{token}.png":{"get":{"tags":["captcha"],"description":"TokenPNG #route:\"GET /{token}.png\"#","parameters":[{"$ref":"#/components/parameters/token.bd2d"}],"responses":{"200":{"description":"#content:\"image/png\"#","content":{"image/png":{}}},"400":{"content":{"text/plain":{"schema":{"type":"string","format":"error"}}}}}}},"/captcha/{token}.wav":{"get":{"tags":["captcha"],"description":"TokenWAV #route:\"GET /{token}.wav\"#","parameters":[{"$ref":"#/components/parameters/token.511c"},{"$ref":"#/components/parameters/lang.c10c"}],"responses":{"200":{"description":"#content:\"audio/x-wav\"#","content":{"audio/x-wav":{}}},"400":{"content":{"text/plain":{"schema":{"type":"string","format":"error"}}}}}}}},"components":{"schemas":{"CaptchaService.64f0":{"type":"object","description":"CaptchaService 图形和语音验证码 #path:\"/captcha\"#\n1. GET /captcha 获取 Token\n2. GET /capture/{token}.png 获取图形验证码 or GET /capture/{token}.wav 获取语音证码\n3. POST /captcha 验证 注意 这个接口仅用于测试\n这些参数也会存在需要图形验证的接口上\n所有 token 验证过一次就失效\n失效后 从1 重新开始"},"Lang.5402":{"enum":["zh","ru","ja","en"],"type":"string"},"Verify.e78c":{"type":"object","properties":{"digits":{"type":"string"},"token":{"type":"string"}}}},"responses":{"err.68d3":{"content":{"text/plain":{"schema":{"type":"string","format":"error"}}}},"png.b978":{"description":"#content:\"image/png\"#","content":{"image/png":{}}},"token.530b":{"content":{"application/json":{"schema":{"type":"string"}}}},"wav.4e5f":{"description":"#content:\"audio/x-wav\"#","content":{"audio/x-wav":{}}}},"parameters":{"lang.c10c":{"name":"lang","in":"query","schema":{"$ref":"#/components/schemas/Lang.5402"}},"token.511c":{"name":"token","in":"path","required":true,"schema":{"type":"string"}},"token.bd2d":{"name":"token","in":"path","required":true,"schema":{"type":"string"}}},"requestBodies":{"verify.458b":{"content":{"application/json":{"schema":{"$ref":"#/components/schemas/Verify.e78c"}}}}}},"tags":[{"name":"captcha","description":"CaptchaService 图形和语音验证码 #path:\"/captcha\"#\n1. GET /captcha 获取 Token\n2. GET /capture/{token}.png 获取图形验证码 or GET /capture/{token}.wav 获取语音证码\n3. POST /captcha 验证 注意 这个接口仅用于测试\n这些参数也会存在需要图形验证的接口上\n所有 token 验证过一次就失效\n失效后 从1 重新开始"}]}`

// RouteOpenAPI
func RouteOpenAPI(router *mux.Router) *mux.Router {
	router.PathPrefix("/swagger/").Handler(http.StripPrefix("/swagger", swaggerui.HandleWith(func(path string) ([]byte, error) {
		if path == "openapi.json" {
			return *(*[]byte)(unsafe.Pointer(&OpenAPI)), nil
		}
		return nil, os.ErrNotExist
	})))
	return router
}
